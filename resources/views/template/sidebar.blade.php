<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="index3.html" class="brand-link">
        <img src="{{ asset('dist/img/AdminLTELogo.png') }}" alt="AdminLTE Logo" class="brand-image img-circle elevation-3" style="opacity: .8">
        <span class="brand-text font-weight-light">AdminLTE 3</span>
    </a>
    
    <!-- Sidebar -->
    <div class="sidebar">
        <!-- Sidebar user panel (optional) -->
        <div class="user-panel mt-3 pb-3 mb-3 d-flex">
            <div class="image">
                <img src="{{ asset('dist/img/user2-160x160.jpg') }}" class="img-circle elevation-2" alt="User Image">
            </div>
            <div class="info">
                <a href="#" class="d-block">Alexander Pierce</a>
            </div>
        </div>
        
        <!-- Sidebar Menu -->
        <nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                <!-- Add icons to the links using the .nav-icon class
                    with font-awesome or any other icon font library -->
                    <li class="nav-item">
                        <a href="{{ route('dashboard') }}" class="nav-link {{ Request::segment(1) == '' ? 'active' : '' }}">
                            <i class="nav-icon fas fa-tachometer-alt"></i>
                            <p>
                                Dashboard
                            </p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="{{ route('calculate') }}" class="nav-link {{ Request::segment(1) == 'calculate' ? 'active' : '' }}">
                            <i class="nav-icon fas fa-subscript"></i> 
                            <p>
                                Calculate
                            </p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="{{ route('result') }}" class="nav-link {{ Request::segment(1) == 'result' ? 'active' : '' }}">
                            <i class="nav-icon fas fa-list"></i>
                            <p>
                                Hasil
                            </p>
                        </a>
                    </li>
                    <li class="nav-header">DATA MASTER</li>
                    <li class="nav-item">
                        <a href="{{ route('master.location') }}" class="nav-link {{ Request::segment(2) == 'location' ? 'active' : '' }}">
                            <i class="nav-icon fas fa-map"></i>
                            <p>
                                Lokasi
                            </p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="{{ route('master.matrix') }}" class="nav-link {{ Request::segment(2) == 'matrix' ? 'active' : '' }}">
                            <i class="nav-icon fas fa-table"></i>
                            <p>
                                Matrik
                            </p>
                        </a>
                    </li>
                </ul>
            </nav>
            <!-- /.sidebar-menu -->
        </div>
        <!-- /.sidebar -->
    </aside>