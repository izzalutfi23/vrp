@extends('template.app')
@section('title', 'Matrik')
@section('container')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Matrik</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{ route('dashboard') }}">Home</a></li>
                        <li class="breadcrumb-item active">Master</li>
                        <li class="breadcrumb-item active">Matrik</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>
    
    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">Data Matrik</h3>
                            <a href="{{ route('convert') }}">
                                <button class="btn btn-primary float-right"><i class="fa fa-random"></i> Convert</button>
                            </a>
                            <a onclick="return confirm('Semua data jarak akan dihapus!')" href="{{ route('master.matrix.delete') }}">
                                <button class="btn btn-danger float-right mr-2"><i class="fa fa-trash"></i> Hapus Semua</button>
                            </a>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            <table id="example1" class="table table-bordered table-striped example1">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Origin</th>
                                        <th>Destination</th>
                                        <th>Distance</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th>No</th>
                                        <th>Origin</th>
                                        <th>Destination</th>
                                        <th>Distance</th>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
</div>
@endsection
@push('style')
<!-- DataTables -->
<link rel="stylesheet" href="{{ asset('plugins/datatables-bs4/css/dataTables.bootstrap4.min.css') }}">
<link rel="stylesheet" href="{{ asset('plugins/datatables-responsive/css/responsive.bootstrap4.min.css') }}">
<link rel="stylesheet" href="{{ asset('plugins/datatables-buttons/css/buttons.bootstrap4.min.css') }}">
@endpush
@push('script')
<!-- DataTables  & Plugins -->
<script src="https://code.jquery.com/jquery-3.5.1.js"></script>
<script src="{{ asset('plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('plugins/datatables-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{ asset('plugins/datatables-responsive/js/dataTables.responsive.min.js') }}"></script>
<script src="{{ asset('plugins/datatables-responsive/js/responsive.bootstrap4.min.js') }}"></script>
<script src="{{ asset('plugins/datatables-buttons/js/dataTables.buttons.min.js') }}"></script>
<script src="{{ asset('plugins/datatables-buttons/js/buttons.bootstrap4.min.js') }}"></script>
<script src="{{ asset('plugins/jszip/jszip.min.js') }}"></script>
<script src="{{ asset('plugins/pdfmake/pdfmake.min.js') }}"></script>
<script src="{{ asset('plugins/pdfmake/vfs_fonts.js') }}"></script>
<script src="{{ asset('plugins/datatables-buttons/js/buttons.html5.min.js') }}"></script>
<script src="{{ asset('plugins/datatables-buttons/js/buttons.print.min.js') }}"></script>
<script src="{{ asset('plugins/datatables-buttons/js/buttons.colVis.min.js') }}"></script>
<script>
    // $(function () {
        //     $("#example1").DataTable({
            //         "responsive": true, "lengthChange": false, "autoWidth": false,
            //         "buttons": ["copy", "csv", "excel", "pdf", "print", "colvis"]
            //     }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');
            //     $('#example2').DataTable({
                //         "paging": true,
                //         "lengthChange": false,
                //         "searching": false,
                //         "ordering": true,
                //         "info": true,
                //         "autoWidth": false,
                //         "responsive": true,
                //     });
                // });
                $(function () {
                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });
                    var table = $('.example1').DataTable({
                        processing: true,
                        serverSide: true,
                        // pageLength: 25,
                        ajax: "{{ url('/master/matrix') }}",
                        columns: [{
                            data: 'DT_RowIndex',
                            name: 'DT_RowIndex'
                        },
                        {
                            data: 'origin',
                            name: 'origin'
                        },
                        {
                            data: 'destination',
                            name: 'destination'
                        },
                        {
                            data: 'distance',
                            name: 'distance'
                        }
                        ]
                    });
                });
            </script>
            @endpush