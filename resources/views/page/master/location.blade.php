@extends('template.app')
@section('title', 'Data Lokasi')
@section('container')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Lokasi</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{ route('dashboard') }}">Home</a></li>
                        <li class="breadcrumb-item active">Master</li>
                        <li class="breadcrumb-item active">Lokasi</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>
    
    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">Data Lokasi</h3>
                            <button class="btn btn-primary float-right ml-2" data-toggle="modal" data-target="#add"><i class="fa fa-plus"></i> Lokasi</button>
                            <button class="btn btn-primary float-right ml-2" data-toggle="modal" data-target="#exampleModal"><i class="fa fa-file-excel"></i> Import</button>
                            <a onclick="return confirm('Semua data lokasi akan dihapus!')" href="{{ route('master.location.delete.all') }}">
                                <button class="btn btn-danger float-right"><i class="fa fa-trash"></i> Hapus Semua</button>
                            </a>
                        </div>
                        <!-- Import -->
                        <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="exampleModalLabel">Import Lokasi</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <form action="{{ route('location.post') }}" method="POST" enctype="multipart/form-data">
                                    @csrf
                                        <div class="modal-body">
                                            <div class="form-group">
                                                <label>File Excel</label>
                                                <input type="file" class="form-control" name="file">
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                            <button type="submit" class="btn btn-primary">Import</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>

                        <!-- Add -->
                        <div class="modal fade" id="add" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="exampleModalLabel">Tambah Lokasi</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <form action="{{ route('master.location.store') }}" method="POST" enctype="multipart/form-data">
                                    @csrf
                                        <div class="modal-body">
                                            <div class="form-group">
                                                <label>Node *</label>
                                                <input required="required" type="number" class="form-control" name="node" placeholder="Masukkan node">
                                            </div>
                                            <div class="form-group">
                                                <label>Lokasi *</label>
                                                <input required="required" type="text" class="form-control" name="location" placeholder="Masukkan nama lokasi">
                                            </div>
                                            <div class="form-group">
                                                <label>Money Casset *</label>
                                                <input required="required" type="number" class="form-control" name="money_casset" placeholder="Masukkan money casset">
                                            </div>
                                            <div class="form-group">
                                                <label>Kurang Dari *</label>
                                                <input required="required" type="time" class="form-control" name="less_than" placeholder="Masukkan waktu kurang dari">
                                            </div>
                                            <div class="form-group">
                                                <label>Cash Out *</label>
                                                <input required="required" type="time" class="form-control" name="cash_out" placeholder="Masukkan waktu cash out">
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                            <button type="submit" class="btn btn-primary">Simpan</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            <table id="example1" class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th>Node</th>
                                        <th>Lokasi</th>
                                        <th>Money Casset</th>
                                        <th>Kurang Dari</th>
                                        <th>Cash Out</th>
                                        <th>Selisih</th>
                                        <th>Aksi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($locations as $loc)
                                    <tr>
                                        <td>{{ $loc->node }}</td>
                                        <td>{{ $loc->location }}</td>
                                        <td>{{ $loc->money_casset }}</td>
                                        <td>{{ $loc->less_than }}</td>
                                        <td>{{ $loc->cash_out }}</td>
                                        <td>{{ $loc->difference }}</td>
                                        <td align="center">
                                            @if($loc->origin->count() < 1 && $loc->destination->count() < 1)
                                            <button class="btn btn-primary btn-sm" data-toggle="modal" data-target="#distance{{ $loc->id }}"><i class="fa fa-plus"></i> Distance</button>
                                            <a onclick="return confirm('Lokasi akan dihapus')" href="{{ route('master.location.delete', $loc->id) }}">
                                                <button class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></button>
                                            </a>
                                            @endif
                                        </td>
                                    </tr>

                                    <!-- Distance -->
                                    @if($loc->origin->count() < 1 && $loc->destination->count() < 1)
                                    <div class="modal fade bd-example-modal-lg" id="distance{{ $loc->id }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                        <div class="modal-dialog modal-lg" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title" id="exampleModalLabel">Menambahkan Jarak ke Masing-masing Lokasi</h5>
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                                <form action="{{ route('master.location.distance', $loc->id) }}" method="POST" enctype="multipart/form-data">
                                                @csrf
                                                @method('patch')
                                                    <div class="modal-body">
                                                        <div class="row">
                                                            @foreach($locations as $l)
                                                            <div class="col-lg-6">
                                                                <div class="form-group">
                                                                    <label>Dari {{ $loc->location }} ke {{ $l->location }} *</label>
                                                                    <input type="number" class="form-control" name="distance[]" value="10" placeholder="25.5">
                                                                </div>
                                                            </div>
                                                            @endforeach
                                                        </div>
                                                        
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                        <button type="submit" class="btn btn-primary">Simpan</button>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                    @endif
                                    @endforeach
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th>Node</th>
                                        <th>Lokasi</th>
                                        <th>Money Casset</th>
                                        <th>Kurang Dari</th>
                                        <th>Cash Out</th>
                                        <th>Selisih</th>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
</div>
@endsection
@push('style')
<!-- DataTables -->
<link rel="stylesheet" href="{{ asset('plugins/datatables-bs4/css/dataTables.bootstrap4.min.css') }}">
<link rel="stylesheet" href="{{ asset('plugins/datatables-responsive/css/responsive.bootstrap4.min.css') }}">
<link rel="stylesheet" href="{{ asset('plugins/datatables-buttons/css/buttons.bootstrap4.min.css') }}">
@endpush
@push('script')
<!-- DataTables  & Plugins -->
<script src="{{ asset('plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('plugins/datatables-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{ asset('plugins/datatables-responsive/js/dataTables.responsive.min.js') }}"></script>
<script src="{{ asset('plugins/datatables-responsive/js/responsive.bootstrap4.min.js') }}"></script>
<script src="{{ asset('plugins/datatables-buttons/js/dataTables.buttons.min.js') }}"></script>
<script src="{{ asset('plugins/datatables-buttons/js/buttons.bootstrap4.min.js') }}"></script>
<script src="{{ asset('plugins/jszip/jszip.min.js') }}"></script>
<script src="{{ asset('plugins/pdfmake/pdfmake.min.js') }}"></script>
<script src="{{ asset('plugins/pdfmake/vfs_fonts.js') }}"></script>
<script src="{{ asset('plugins/datatables-buttons/js/buttons.html5.min.js') }}"></script>
<script src="{{ asset('plugins/datatables-buttons/js/buttons.print.min.js') }}"></script>
<script src="{{ asset('plugins/datatables-buttons/js/buttons.colVis.min.js') }}"></script>
<script>
    $(function () {
        $("#example1").DataTable({
            "order": [[ 0, "desc" ]],
        });
    });
</script>
@endpush