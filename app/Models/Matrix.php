<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Matrix extends Model
{
    use HasFactory;

    protected $table = 'matrics';
    protected $fillable = ['origin', 'destination', 'distance'];

    public function originLoc(){
        return $this->belongsTo(Location::class, 'origin', 'node');
    }

    public function destinationLoc(){
        return $this->belongsTo(Location::class, 'destination', 'node');
    }
}
