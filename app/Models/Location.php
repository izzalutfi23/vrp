<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Location extends Model
{
    use HasFactory;

    protected $fillable = ['node', 'location', 'money_casset', 'less_than', 'cash_out', 'difference', 'value'];

    public function origin(){
        return $this->hasMany(Matrix::class, 'origin', 'node');
    }

    public function destination(){
        return $this->hasMany(Matrix::class, 'destination', 'node');
    }
}
