<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\{Location, Matrix};
use Illuminate\Support\Facades\DB;

class CalculateController extends Controller
{
    public $kecepatan = 50;// 50KM/jam
    public $waktuPelayanan = 15;// 15 menit
    public $a1 = 0.1;
    public $a2 = 0.9;
    public $lamda = 1;
    public $kapasitas = 14;
    public $maxTime = 480;
    public $waktuSetup = 15;//15 Menit
    public $kapasitasMoneyCasset = 56;
    public $waktuReplenishment = 15;
    public $startWork = '07:00:00';
    public $timestampstartwork;// = strtotime($this->startWork);
    public $lasttime;// $timestampstartwork;
    public $allNodesAdded = array();
    public $rute = [0];
    public $locations;
    public $matrixs;
    public $tmpKapasitasMoneyCasset;
    public $newDetailsRoute = [];
    public $waktuKedatanganMin = '';   
    public $lastWaktuKeberangkatanMin;
    // public $lamaPerjalananMin = 0;    
    public $totalWaktuPerjalanan =0;
    public $totalJamKerja =0;
    public $totaljarakTempuh =0;
    public $first = 1;
    public $tourIndex = 0;
    public $tour = array();

    public function __construct(){
        $this->timestampstartwork = strtotime($this->startWork);
        $this->lastWaktuKeberangkatanMin = $this->startWork;
        $this->lasttime = $this->timestampstartwork;
        $this->locations = Location::all();
        $this->matrixs = Matrix::all();
        $this->tmpKapasitasMoneyCasset = $this->kapasitasMoneyCasset;

   }
    public function index(){
        // return phpinfo();
    
        // return  $this->findByOrigin($this->matrixs, 7);
        //Menghitung Waktu Tempuh masing-masing jarak

        foreach($this->matrixs as $m => $v){
            $time = $v->distance/$this->kecepatan*60;
            $v['waktu_tempuh'] = ceil($time);
        }

        // return $matrixs;
        // print_r(json_encode($matrixs));

        $next = 1;
        
        
        $origin = 0;
        $i = 0;
        
        $this->allNodesAdded[] =$i;

        $totalAllJamkerja = 0;

        $newRute = [0];
        
        $nodeMin = 0;
        // $waktuKedatanganMin = '';   
        // $waktuKeberangkatanMin = '';
        // $lamaPerjalananMin = 0;       

        while(sizeof(array_unique($this->allNodesAdded)) < sizeof($this->locations)){

            echo "<h4>===Tour ".($this->tourIndex+1)."===</h4>";
            $lastTimeMin = strtotime($this->lastWaktuKeberangkatanMin);
            
            $this->waktuKeberangkatanMin = date('H:i:s', $lastTimeMin);
            $this->newDetailsRoute = [];

            $tmpData = (object) array(
                'lokasi' => $this->rute[$i],
                'jadwal30' => '-',
                'cash_out' => '-', 
                'waktu_tempuh' => 0,
                'waktu_kedatangan' => '00:00:00',
                'waktu_keberangkatan' => $this->waktuKeberangkatanMin,
                'waktu_replenishment' => 0,
                'money_casset' => 0,
                'kapasitas_money_casset' => $this->tmpKapasitasMoneyCasset,
                'total_waktu_perjalanan' => 0,
                'total_jam_kerja' => 0,
                'total_all_waktu' => 0,
                'total_jarak_tempuh' => 0
            );

            $this->newDetailsRoute[] = $tmpData;

            // return $this->newDetailsRoute;
            $iterasi = 0;
            while (1){
              
                //menentukan jarak terdekat dari depot
                $this->first = 1;
                $min = -1;
                $origin = $this->rute[sizeof($this->rute)-1];
                $byOrigin = $this->findByOrigin($this->matrixs, $origin);

                if($this->checkRute() || sizeof(array_unique($this->allNodesAdded)) >= sizeof($this->locations)) {
                    // print(json_encode($this->rute)." <br>");
                    break;
                }
                
                echo "<h4>=== Iterasi ".(++$iterasi)."===</h4>";
                // Minimasi Fungsi Tujuan
                $this->minimasiFunc($byOrigin, $this->lastWaktuKeberangkatanMin , $this->kapasitasMoneyCasset, " JARAK");

               
                // return $this->newDetailsRoute;

                // ngitung Zjarak dan Zwaktu
                $zJarak = [];
                $zWaktu = [];
                $z12 = [];

                $nodeMin = $this->rute[sizeof($this->rute)-1];

                foreach($this->locations as $l => $v){
                    if($l == 0) continue;
                    //bobot jarak
                    $dIU = $this->findDistance($this->matrixs, $nodeMin, $l);//$tmp->distance;
                    $dU0 = $this->findDistance($this->matrixs, $l, 0);//$tmp->distance;
                    $dI0 = $this->findDistance($this->matrixs, 0, $nodeMin);//$tmp->distance;

                    $zd = $dIU+$dU0-$dI0;

                    $zTmp = (object) array(
                        'node' => $l,
                        'diu' => $dIU,
                        'dU0' => $dU0,
                        'dI0' => $dI0,
                        'zd' => $zd
                    );
                    $zJarak[] = $zTmp;
                    //end bobot jarak

                    //perhitungan bobot waktu
                    $zTimetmp = (object) array(
                        'node' => $l,
                        'tiu' => 0,
                        'tU0' => 0,
                        'tI0' => 0,
                        'zt' => 0
                    );

                    $isloop = 0;
                    foreach($this->matrixs as $m => $mv){
                        // $isloop = 1;
                        if($mv->origin == $nodeMin && $mv->destination == $l){
                            $zTimetmp->tiu = $mv->waktu_tempuh;
                            $isloop++;
                        }
                        if($mv->origin == $l && $mv->destination == 0){
                            $zTimetmp->tU0 = $mv->waktu_tempuh;
                            $isloop++;
                        }
                        if($mv->origin == $nodeMin && $mv->destination == 0){
                            $zTimetmp->tI0 = $mv->waktu_tempuh;
                            $isloop++;
                        }

                        
                        $zTimetmp->zt = ($zTimetmp->tiu + $zTimetmp->tU0 + $this->waktuPelayanan)-$zTimetmp->tI0;
                        if( $isloop >= 3) {
                            $zWaktu[] = $zTimetmp;
                            break;
                        }
                        // echo $zTimetmp->zT."<br>";
                    }
                }

                // print_r(json_encode($zJarak));
                // echo "<br>";
                // print_r(json_encode($zWaktu));

                //Menghitung besarnya ongkos penyisipan yang besarnya proporsional terhadap tambahan jarak dan tambahan waktu tempuh 
                foreach($zJarak as $j => $v){
                    if(in_array($v->node, $this->allNodesAdded)) continue;
                    $z1 = ($this->a1*$v->zd) + ($this->a2*$zWaktu[$j]->zt);
                    $z2 = ($this->lamda * $v->dU0)-$z1;
                    // echo $z1." ".$z2."<br>";
                    $z12[] = (object) array(
                        'node' => $v->node,
                        'destination' => $v->node,
                        'z1' => $z1,
                        'z2' => $z2
                    );
                    
                }

                //check if sudah memenuhi kapasitas || waktu terpenuhi

                // urutkan berdasarkan terkecil
                usort($z12,function($first,$second){
                    return $first->z1 > $second->z1;
                });
                // print_r(json_encode($z12));
                // echo "<br>";
                // print(json_encode($this->rute)." <br>");
                // print_r(json_encode($this->allNodesAdded));
                if($this->checkRute() || sizeof(array_unique($this->allNodesAdded)) >= sizeof($this->locations)) {
                    // print(json_encode($this->rute)." <br>");
                    break;
                }
                // Minimasi Fungsi Tujuan
                $this->minimasiFunc($z12, $this->lastWaktuKeberangkatanMin, $this->kapasitasMoneyCasset, "Z1");


                usort($z12,function($first,$second){
                    return $first->z2 < $second->z2;
                });
                // print_r(json_encode($z12));
                // print(json_encode($this->rute)." <br>");
                // print_r(json_encode($this->allNodesAdded));
                if($this->checkRute() || sizeof(array_unique($this->allNodesAdded)) >= sizeof($this->locations)) {
                    // print(json_encode($this->rute)." <br>");
                    break;
                }
                // Minimasi Fungsi Tujuan
                $this->minimasiFunc($z12, $this->lastWaktuKeberangkatanMin, $this->kapasitasMoneyCasset, "Z2");
            }
        }
        
        // print_r($this->tourIndex);
        // var_dump($this->allNodesAdded);
        // print_r(json_encode($this->allNodesAdded));
        // print_r(array_unique($this->allNodesAdded));


        foreach($this->tour as $t => $tv){//tour

            // print_r(json_encode($tv));
            
            echo "<h3>Tour Ke ".($t+1)."</h3>";
            // print_r(json_encode($tv));
            foreach($tv as $r => $rv){ // Rute 
                echo "<h4>Rute Ke ".($r+1)."</h4>";
                echo "<table border='1' style='border-collapse: collapse'><tr>
                <th>Lokasi</th>
                <th>Jadwal Pengiriman 30%</th>
                <th>Jadwal Cashout</th>
                <th>Lama Perjalanan (Menit)</th>
                <th>Waktu Kedatangan</th>
                <th>Waktu Proses Replenishment (Menit)</th>
                <th>Waktu Keberangkatan Kendaraan</th>
                <th>Money Casset Terpakai</th>
                <th>Kapasitas Money Casset</th>";
                echo "<tbody>";

                $totalWaktuPerjalanan = 0;
                $totalJamKerja = 0;
                $totaljarakTempuh = 0;
                foreach($rv as $d => $dv){
                    echo "<tr>
                    <td>".$dv->lokasi."</td>
                    <td>".$dv->jadwal30."</td>
                    <td>".$dv->cash_out."</td>
                    <td>".$dv->waktu_tempuh."</td>
                    <td>".$dv->waktu_kedatangan."</td>
                    <td>".$dv->waktu_replenishment."</td>
                    <td>".$dv->waktu_keberangkatan."</td>
                    <td>".$dv->money_casset."</td>
                    <td>".$dv->kapasitas_money_casset."</td></tr>";

                    $totalWaktuPerjalanan = $dv->total_waktu_perjalanan;
                    $totalJamKerja = $dv->total_all_waktu;
                    $totaljarakTempuh = $dv->total_jarak_tempuh;
                }
                echo "</tbody>";
                echo "<tfoot>
                <tr>
                <td colspan='2'>Total Perjalanan (Menit)</td>
                <td >".$totalWaktuPerjalanan."</td>
                <td colspan='2'>Total Jam Kerja (Menit)</td>
                <td >".$totalJamKerja."</td>
                <td></td>
                <td></td>
                <td></td>
                </tr>
                <tr>
                <td colspan='2'>Total Jarak Tempuh (Km)</td>
                <td>".$totaljarakTempuh."</td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                </tr>
                </tfoot></table>";
                // foreach($tv-> )
            }
           
           
        }
        return null;
        return $this->tour;
//=====
        return view('page.calculate.index');
    }

    function minutes($time){
        $time = explode(':', $time);
        return ($time[0]*60) + ($time[1]) + ($time[2]/60);
    }

    function findWaktuTempuh($matrixs, $from, $to){
        foreach($matrixs as $m => $mv){
            if($mv->origin == $from && $mv->destination == $to){
                return $mv->waktu_tempuh;
            }
        }
    }
    
    function findWaktuFromNode($detailsRoute, $node){
        foreach($detailsRoute as $d => $dv){
            if($dv->lokasi == $node) return $dv;
        }
    }

    function findBobotWaktuMinimasi($lessthan, $cashout, $waktuKedatangan){
        $tmpLesthan = strtotime($lessthan);
        $tmpCashout = strtotime($cashout);
        $tmpKedatangan = strtotime($waktuKedatangan);

// echo $lessthan." ".$waktuKedatangan." = ". $tmpLesthan-$tmpKedatangan;//." ".($tmpKedatangan >= $tmpLesthan && $tmpKedatangan < $tmpCashout);

$bobot = 0;
        if(($tmpLesthan - $tmpKedatangan) > 3600) $bobot = 3; //kedatangan > 1jam sebelum 30%
        if(($tmpLesthan - $tmpKedatangan) <= 3600) $bobot = 2; //kedatangan <= 1jam sebelum 30%
        if($tmpKedatangan >= $tmpLesthan && $tmpKedatangan < $tmpCashout) $bobot = 1;
        if($tmpKedatangan == $tmpCashout) $bobot = 0;
// echo " ".$bobot." ";
        return $bobot;
    }

    function findDistance($matrix, $origin, $destination){
        foreach($matrix as $m){
            if($m->origin == $origin && $m->destination == $destination){
                return $m->distance;
            }
            elseif($m->origin == $destination && $m->destination == $origin){
                return $m->distance;
            }
        }
    }

    function findByOrigin($matrix, $origin){
        $temp = [];
        foreach($matrix as $m){
            if($m->origin == $m->destination) continue;

           
            if($m->origin == $origin){
                if(!in_array($m->destination, $this->allNodesAdded)) {
                    $temp[] = $m;
                }
            }
            
            
        }

        return $temp;
    }

    function checkAllNodes($nodes){
        foreach($this->allNodesAdded as $m => $v){

            if($nodes == $v) return true;
        }
        return false;
    }

    function checkRute(){
        $isBreak = false;
        //uji jika perjalanan kedepot

        // print_r($this->rute[sizeof($this->rute)-1]." ");
        // print_r( $this->newDetailsRoute[sizeof($this->newDetailsRoute)-1]->total_all_waktu." ");
        // print_r(sizeof($this->rute)." ");
        $lamaPerjalanan = $this->findDistance($this->matrixs, $this->rute[sizeof($this->rute)-1], 0);;
        $lamaPerjalanan = ceil($lamaPerjalanan/$this->kecepatan*60);

        $tmpJamKerja = $this->newDetailsRoute[sizeof($this->newDetailsRoute)-1]->total_all_waktu + $lamaPerjalanan;


        if(sizeof($this->rute) > $this->kapasitas && $tmpJamKerja < $this->maxTime) {
            //Tur sama rute baru
            $isBreak = true;
            // print_r("1");

            $this->totalWaktuPerjalanan = $this->newDetailsRoute[sizeof($this->newDetailsRoute)-1]->total_waktu_perjalanan + $lamaPerjalanan;//tambah total perjalanan 
            $this->totalJamKerja = $this->newDetailsRoute[sizeof($this->newDetailsRoute)-1]->total_jam_kerja + $this->waktuSetup;//tambah jam kerja
            $this->totalJarakTempuh = ($this->kecepatan*$this->totalWaktuPerjalanan/60);

            $waktuKedatangan = date('H:i:s', strtotime($this->newDetailsRoute[sizeof($this->newDetailsRoute)-1]->waktu_keberangkatan) + $lamaPerjalanan*60);
            $startWork = date('H:i:s', strtotime($waktuKedatangan) + $this->waktuSetup*60);
            $waktuKeberangkatan = $startWork;

            $tmpData = (object) array(
                'lokasi' => 0,
                'jadwal30' => $this->locations[0]->less_than,
                'cash_out' => $this->locations[0]->cash_out, 
                'waktu_tempuh' => $lamaPerjalanan,
                'waktu_kedatangan' => $waktuKedatangan,
                'waktu_keberangkatan' => $waktuKeberangkatan,
                'waktu_replenishment' => $this->waktuReplenishment,
                'money_casset' => $this->locations[0]->money_casset,
                'kapasitas_money_casset' => $this->tmpKapasitasMoneyCasset,
                'total_waktu_perjalanan' => $this->totalWaktuPerjalanan, 
                'total_jam_kerja' => $this->totalJamKerja, 
                'total_all_waktu' => ($this->totalWaktuPerjalanan+$this->totalJamKerja), 
                'total_jarak_tempuh' => $this->totalJarakTempuh
            );

            // $this->lastWaktuKeberangkatanMin = $waktuKedatangan;
            $this->newDetailsRoute[] = $tmpData;//add temporary Details route;
            $this->rute[] = 0; //node 1

            // print_r("Tour ".($this->tourIndex+1)." -> ".json_encode($this->rute));
            // Tour Baru 
            $this->tour[$this->tourIndex][] = $this->newDetailsRoute;
            $this->rute = [0];
            // echo "tur baru";
            // $this->tourIndex++;

            // $startWork = date('H:i:s', strtotime($waktuKedatangan) + $this->waktuSetup*60);
            $lasttime = strtotime($startWork);
            $this->timestampstartwork = strtotime($startWork);
            $this->lastWaktuKeberangkatanMin = $startWork;
            $this->lasttime = $this->timestampstartwork;
            $this->tmpKapasitasMoneyCasset = $this->kapasitasMoneyCasset;

        }
        else if(sizeof($this->rute) <= $this->kapasitas && $tmpJamKerja > $this->maxTime){
            // Tur baru rute baru
            $isBreak = true;
            print_r("2");

            // unset($this->newDetailsRoute[sizeof($this->newDetailsRoute)-1]);
            // unset($this->rute[sizeof($this->rute)-1]);
            // unset($this->allNodesAdded[sizeof($this->allNodesAdded)-1]);
            array_splice($this->newDetailsRoute,sizeof($this->newDetailsRoute)-1, 1 );
            array_splice($this->rute, sizeof($this->rute)-1, 1);
            array_splice($this->allNodesAdded, sizeof($this->allNodesAdded)-1, 1);

            $lamaPerjalanan = $this->findDistance($this->matrixs, $this->rute[sizeof($this->rute)-1], 0);;
            $lamaPerjalanan = ceil($lamaPerjalanan/$this->kecepatan*60);
            
            $this->totalWaktuPerjalanan = $this->newDetailsRoute[sizeof($this->newDetailsRoute)-1]->total_waktu_perjalanan + $lamaPerjalanan;//tambah total perjalanan 
            $this->totalJamKerja = $this->newDetailsRoute[sizeof($this->newDetailsRoute)-1]->total_jam_kerja;//tambah jam kerja
            $this->totalJarakTempuh = ($this->kecepatan*$this->totalWaktuPerjalanan/60);

            $waktuKedatangan = date('H:i:s', strtotime($this->newDetailsRoute[sizeof($this->newDetailsRoute)-1]->waktu_keberangkatan) + $lamaPerjalanan*60);

            $tmpData = (object) array(
                'lokasi' => 0,
                'jadwal30' => $this->locations[0]->less_than,
                'cash_out' => $this->locations[0]->cash_out, 
                'waktu_tempuh' => $lamaPerjalanan,
                'waktu_kedatangan' => $waktuKedatangan,
                'waktu_keberangkatan' => '00:00:00',
                'waktu_replenishment' => 0,//$waktuReplenishment,
                'money_casset' => $this->locations[0]->money_casset,
                'kapasitas_money_casset' => $this->tmpKapasitasMoneyCasset,
                'total_waktu_perjalanan' => $this->totalWaktuPerjalanan, 
                'total_jam_kerja' => $this->totalJamKerja, 
                'total_all_waktu' => ($this->totalWaktuPerjalanan+$this->totalJamKerja), 
                'total_jarak_tempuh' => $this->totalJarakTempuh
            );

            $this->lastWaktuKeberangkatanMin = $waktuKedatangan;
            $this->newDetailsRoute[] = $tmpData;//add temporary Details route;
            $this->rute[] = 0; //node 1

            // print_r("Tour ".($this->tourIndex+1)." -> ".json_encode($this->rute));

            // Tour Baru 
            $this->tour[$this->tourIndex][] = $this->newDetailsRoute;
            $this->rute = [0];
            // echo "tur baru";
            $this->tourIndex++;
            
            $this->startWork = '07:00:00';
            $this->timestampstartwork = strtotime($this->startWork);
            $this->lastWaktuKeberangkatanMin = $this->startWork;
            $this->lasttime = $this->timestampstartwork;
            $this->tmpKapasitasMoneyCasset = $this->kapasitasMoneyCasset;

            $this->totalWaktuPerjalanan = 0;
            $this->totalJamKerja = 0;
            $this->totalJarakTempuh = 0;
        }
        else if((sizeof($this->rute) <= $this->kapasitas || sizeof($this->rute) > $this->kapasitas) && $tmpJamKerja == $this->maxTime){
            // tur baru rute baru tanpa hapus rute sembulnya 
            $isBreak = true;
            // print_r("3");

            $this->totalWaktuPerjalanan = $this->newDetailsRoute[sizeof($this->newDetailsRoute)-1]->total_waktu_perjalanan + $lamaPerjalanan;//tambah total perjalanan 
            $this->totalJamKerja = $this->newDetailsRoute[sizeof($this->newDetailsRoute)-1]->total_jam_kerja;//tambah jam kerja
            $this->totalJarakTempuh = ($this->kecepatan*$this->totalWaktuPerjalanan/60);

            $waktuKedatangan = date('H:i:s', strtotime($this->newDetailsRoute[sizeof($this->newDetailsRoute)-1]->waktu_keberangkatan) + $lamaPerjalanan*60);

            $tmpData = (object) array(
                'lokasi' => 0,
                'jadwal30' => $this->locations[0]->less_than,
                'cash_out' => $this->locations[0]->cash_out, 
                'waktu_tempuh' => $lamaPerjalanan,
                'waktu_kedatangan' => $waktuKedatangan,
                'waktu_keberangkatan' => '00:00:00',
                'waktu_replenishment' => 0,//$waktuReplenishment,
                'money_casset' => $this->locations[0]->money_casset,
                'kapasitas_money_casset' => $this->tmpKapasitasMoneyCasset,
                'total_waktu_perjalanan' => $this->totalWaktuPerjalanan, 
                'total_jam_kerja' => $this->totalJamKerja, 
                'total_all_waktu' => ($this->totalWaktuPerjalanan+$this->totalJamKerja), 
                'total_jarak_tempuh' => $this->totalJarakTempuh
            );

            $this->lastWaktuKeberangkatanMin = $waktuKedatangan;
            $this->newDetailsRoute[] = $tmpData;//add temporary Details route;
            $this->rute[] = 0; //node 1

            // print_r("Tour ".($this->tourIndex+1)." -> ".json_encode($this->rute));

            // Tour Baru 
            $this->tour[$this->tourIndex][] = $this->newDetailsRoute;
            $this->rute = [0];
            // echo "tur baru";
            $this->tourIndex++;
            
            $this->startWork = '07:00:00';
            $this->timestampstartwork = strtotime($this->startWork);
            $this->lastWaktuKeberangkatanMin = $this->startWork;
            $this->lasttime = $this->timestampstartwork;
            $this->tmpKapasitasMoneyCasset = $this->kapasitasMoneyCasset;

            $this->totalWaktuPerjalanan = 0;
            $this->totalJamKerja = 0;
            $this->totalJarakTempuh = 0;
        }
        else if((sizeof($this->rute) <= $this->kapasitas || sizeof($this->rute) > $this->kapasitas) && $tmpJamKerja > $this->maxTime) {
            //Tur Baru rute baru
            $isBreak = true;
            // print_r("5");
            
            // unset($this->newDetailsRoute[sizeof($this->newDetailsRoute)-1]);
            // unset($this->rute[sizeof($this->rute)-1]);
            // unset($this->allNodesAdded[sizeof($this->allNodesAdded)-1]);

            array_splice($this->newDetailsRoute,sizeof($this->newDetailsRoute)-1, 1 );
            array_splice($this->rute, sizeof($this->rute)-1, 1);
            array_splice($this->allNodesAdded, sizeof($this->allNodesAdded)-1, 1);

            $lamaPerjalanan = $this->findDistance($this->matrixs, $this->rute[sizeof($this->rute)-1], 0);;
            $lamaPerjalanan = ceil($lamaPerjalanan/$this->kecepatan*60);
            
            $this->totalWaktuPerjalanan = $this->newDetailsRoute[sizeof($this->newDetailsRoute)-1]->total_waktu_perjalanan + $lamaPerjalanan;//tambah total perjalanan 
            $this->totalJamKerja = $this->newDetailsRoute[sizeof($this->newDetailsRoute)-1]->total_jam_kerja;//tambah jam kerja
            $this->totalJarakTempuh = ($this->kecepatan*$this->totalWaktuPerjalanan/60);

            $waktuKedatangan = date('H:i:s', strtotime($this->newDetailsRoute[sizeof($this->newDetailsRoute)-1]->waktu_keberangkatan) + $lamaPerjalanan*60);

            $tmpData = (object) array(
                'lokasi' => 0,
                'jadwal30' => $this->locations[0]->less_than,
                'cash_out' => $this->locations[0]->cash_out, 
                'waktu_tempuh' => $lamaPerjalanan,
                'waktu_kedatangan' => $waktuKedatangan,
                'waktu_keberangkatan' => '00:00:00',
                'waktu_replenishment' => 0,//$waktuReplenishment,
                'money_casset' => $this->locations[0]->money_casset,
                'kapasitas_money_casset' => $this->tmpKapasitasMoneyCasset,
                'total_waktu_perjalanan' => $this->totalWaktuPerjalanan, 
                'total_jam_kerja' => $this->totalJamKerja, 
                'total_all_waktu' => ($this->totalWaktuPerjalanan+$this->totalJamKerja), 
                'total_jarak_tempuh' => $this->totalJarakTempuh
            );

            $this->lastWaktuKeberangkatanMin = $waktuKedatangan;
            $this->newDetailsRoute[] = $tmpData;//add temporary Details route;
            $this->rute[] = 0; //node 1

            // print_r("Tour ".($this->tourIndex+1)." -> ".json_encode($this->rute));

            // Tour Baru 
            $this->tour[$this->tourIndex][] = $this->newDetailsRoute;
            $this->rute = [0];
            // echo "tur baru";
            $this->tourIndex++;
            
            $this->startWork = '07:00:00';
            $this->timestampstartwork = strtotime($this->startWork);
            $this->lastWaktuKeberangkatanMin = $this->startWork;
            $this->lasttime = $this->timestampstartwork;
            $this->tmpKapasitasMoneyCasset = $this->kapasitasMoneyCasset;

            $this->totalWaktuPerjalanan = 0;
            $this->totalJamKerja = 0;
            $this->totalJarakTempuh = 0;
        }
        elseif(sizeof($this->rute) <= $this->kapasitas && 
        $tmpJamKerja < $this->maxTime && 
        sizeof(array_unique($this->allNodesAdded)) == sizeof($this->locations)){
            //Tur sama rute baru
            $isBreak = true;
            // print_r("1");

            $this->totalWaktuPerjalanan = $this->newDetailsRoute[sizeof($this->newDetailsRoute)-1]->total_waktu_perjalanan + $lamaPerjalanan;//tambah total perjalanan 
            $this->totalJamKerja = $this->newDetailsRoute[sizeof($this->newDetailsRoute)-1]->total_jam_kerja ;//tambah jam kerja
            $this->totalJarakTempuh = ($this->kecepatan*$this->totalWaktuPerjalanan/60);

            $waktuKedatangan = date('H:i:s', strtotime($this->newDetailsRoute[sizeof($this->newDetailsRoute)-1]->waktu_keberangkatan) + $lamaPerjalanan*60);
            $startWork = date('H:i:s', strtotime($waktuKedatangan) + $this->waktuSetup*60);
            $waktuKeberangkatan = $startWork;

            $tmpData = (object) array(
                'lokasi' => 0,
                'jadwal30' => $this->locations[0]->less_than,
                'cash_out' => $this->locations[0]->cash_out, 
                'waktu_tempuh' => $lamaPerjalanan,
                'waktu_kedatangan' => $waktuKedatangan,
                'waktu_keberangkatan' => 0,
                'waktu_replenishment' => 0,
                'money_casset' => $this->locations[0]->money_casset,
                'kapasitas_money_casset' => $this->tmpKapasitasMoneyCasset,
                'total_waktu_perjalanan' => $this->totalWaktuPerjalanan, 
                'total_jam_kerja' => $this->totalJamKerja, 
                'total_all_waktu' => ($this->totalWaktuPerjalanan+$this->totalJamKerja), 
                'total_jarak_tempuh' => $this->totalJarakTempuh
            );

            // $this->lastWaktuKeberangkatanMin = $waktuKedatangan;
            $this->newDetailsRoute[] = $tmpData;//add temporary Details route;
            $this->rute[] = 0; //node 1

            // print_r("Tour ".($this->tourIndex+1)." -> ".json_encode($this->rute));
            // Tour Baru 
            $this->tour[$this->tourIndex][] = $this->newDetailsRoute;
            $this->rute = [0];
            // echo "tur baru";
            // $this->tourIndex++;

            // $startWork = date('H:i:s', strtotime($waktuKedatangan) + $this->waktuSetup*60);
            $lasttime = strtotime($startWork);
            $this->timestampstartwork = strtotime($startWork);
            $this->lastWaktuKeberangkatanMin = $startWork;
            $this->lasttime = $this->timestampstartwork;
            $this->tmpKapasitasMoneyCasset = $this->kapasitasMoneyCasset;
        }
       
        return $isBreak;
    }



    function minimasiFunc($byOrigin, $startWork, $kapasitasMoneyCasset, $title){
        if($this->first == 1){
            usort($byOrigin,function($first,$second){
                return $first['distance'] > $second['distance'];
            });

            // if(sizeof($this->tour) == 1){
                echo json_encode($byOrigin)."<br>";
            // }
        }
        

        $isloop =1;
        $i = 0;
        $tmpMin = 1000;

        $startTime = strtotime($startWork);//+($waktuReplenishment*60);

        $nodeMin = 0;
        $t = 0;

        echo "<h3>Minimasi Fungsi ".$title."</h3>";
        foreach($byOrigin as $d => $v){
          
            // if(in_array($v->destination, $this->rute)) continue;
            if(in_array($v->destination, $this->allNodesAdded)) {
                continue;
            }

            if($this->first != 1) $v->distance = $this->findDistance($this->matrixs, $this->rute[sizeof($this->rute)-1], $v->node);
        
            $lamaPerjalanan = $v->distance;
            $lamaPerjalanan = ceil($lamaPerjalanan/$this->kecepatan*60);
            $waktuKedatangan = date('H:i:s', $startTime + $lamaPerjalanan*60);

            $bobot = $this->findBobotWaktuMinimasi($this->locations[$v->destination]->less_than, $this->locations[$v->destination]->cash_out,  $waktuKedatangan);

            $res = ($lamaPerjalanan*1) + (10*$bobot);
            
            echo "Lokasi ".$v->destination." nilai : ".$res."<br>";
            
            if($res < $tmpMin){
                $tmpMin = $res;
                $nodeMin = $v->destination;
                $lamaPerjalananMin = $lamaPerjalanan;
                $tmpLasttime = $startTime + $lamaPerjalanan*60;

                $waktuKedatanganMin = $waktuKedatangan;   
            }
            
        }

        echo "nilai terkecil =>";
        echo "lokasi : ".$nodeMin." nilai : ".$tmpMin."<br>";

        $this->tmpKapasitasMoneyCasset -= $this->locations[$v->destination]->money_casset;

        // $waktuKedatanganMin = $waktuKedatangan;   
        $waktuKeberangkatanMin = date('H:i:s', $tmpLasttime+$this->waktuReplenishment*60);

        $this->totalWaktuPerjalanan += $lamaPerjalananMin;//tambah total perjalanan 
        $this->totalJamKerja += $this->waktuReplenishment;//tambah jam kerja
        $this->totalJarakTempuh = ($this->kecepatan*$this->totalWaktuPerjalanan/60);

        $tmpData = (object) array(
            'lokasi' => $nodeMin,
            'jadwal30' => $this->locations[$nodeMin]->less_than,
            'cash_out' => $this->locations[$nodeMin]->cash_out, 
            'waktu_tempuh' => $lamaPerjalananMin,
            'waktu_kedatangan' => $waktuKedatanganMin,
            'waktu_keberangkatan' => $waktuKeberangkatanMin,
            'waktu_replenishment' => $this->waktuReplenishment,
            'money_casset' => $this->locations[$v->destination]->money_casset,
            'kapasitas_money_casset' => $this->tmpKapasitasMoneyCasset,
            'total_waktu_perjalanan' => $this->totalWaktuPerjalanan, 
            'total_jam_kerja' => $this->totalJamKerja, 
            'total_all_waktu' => ($this->totalWaktuPerjalanan+$this->totalJamKerja), 
            'total_jarak_tempuh' => $this->totalJarakTempuh
        );

        $this->lastWaktuKeberangkatanMin = $waktuKeberangkatanMin;
        $this->newDetailsRoute[] = $tmpData;//add temporary Details route;

        $this->rute[] = $nodeMin; //node 1
        $this->allNodesAdded[] =$nodeMin;
        $this->first = 0;
    }
}






