<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\{Location, Matrix};
use Alert;
use Illuminate\Support\Facades\DB;

class LocationController extends Controller
{
    public function index(){
        $locations = Location::all();
        return view('page.master.location', compact('locations'));
        // for($x=0; $x<1000000; $x++){
        //     Matrix::create([
        //         'origin' => '1',
        //         'destination' => '2',
        //         'distance' => '50'
        //     ]);
        // }
    }

    public function store(Request $request){
        $diff = $this->getTimeDiff($request->less_than, $request->cash_out);
        $check = Location::where('node', $request->node)->first();
        if($check){
            Alert::error('Gagal', 'Node sudah dipakai!');
            return redirect()->back();
        }
        else{
            $data = [];
            Location::create([
                'node' => $request->node,
                'location' => $request->location,
                'money_casset' => $request->money_casset,
                'less_than' => $request->less_than,
                'cash_out' => $request->cash_out,
                'difference' => $diff,
                'value' => json_encode($data)
            ]);

            Alert::success('Berhasil', 'Lokasi baru berhasil ditambahkan!');
            return redirect()->back();
        }
    }

    public function storeDistance(Request $request, $id){
        $d = [];
        foreach($request->distance as $dis){
            array_push($d, (int)$dis);
        }

        Location::whereId($id)->update([
            'value' => json_encode($d)
        ]);

        $location = Location::whereId($id)->first();
        $locations = Location::all();
        foreach($locations as $loc){
            if($loc->id != $location->id){
                $val = json_decode($loc->value);
                array_push($val, (int)$request->distance[$loc->node]);
                Location::whereId($loc->id)->update([
                    'value' => json_encode($val)
                ]);

                Matrix::create([
                    'origin' => $loc->node,
                    'destination' => $location->node,
                    'distance' => (int)$request->distance[$loc->node]
                ]);
            }
        }

        $arr = json_decode($location->value);
        foreach($arr as $key => $v){
            $index = Location::select('id', 'node', 'location')->where('node', $key)->first();
            Matrix::create([
                'origin' => $location->node,
                'destination' => $index->node,
                'distance' => $v
            ]);
        }

        Alert::success('Success', 'Distance berhasil ditambahkan!');
        return redirect()->back();
    }

    public function destroy($id){
        Location::whereId($id)->delete();
        Alert::success('Success', 'Lokasi berhasil dihapus!');
        return redirect()->back();
    }

    public function destroyAll(){
        DB::table('matrics')->truncate();
        DB::table('locations')->delete();
        Alert::success('Success', 'Lokasi berhasil dihapus!');
        return redirect()->back();
    }

    public function getTimeDiff($dtime,$atime)
    {
        $nextDay = $dtime>$atime?1:0;
        $dep = explode(':',$dtime);
        $arr = explode(':',$atime);
        $diff = abs(mktime($dep[0],$dep[1],0,date('n'),date('j'),date('y'))-mktime($arr[0],$arr[1],0,date('n'),date('j')+$nextDay,date('y')));
        $hours = floor($diff/(60*60));
        $mins = floor(($diff-($hours*60*60))/(60));
        $secs = floor(($diff-(($hours*60*60)+($mins*60))));
        if(strlen($hours)<2){$hours="0".$hours;}
        if(strlen($mins)<2){$mins="0".$mins;}
        if(strlen($secs)<2){$secs="0".$secs;}
        return $hours.':'.$mins.':'.$secs;
    }
}
