<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\{Location, Matrix};
use Illuminate\Support\Facades\DB;

class CalculateController extends Controller
{
    

    public function index(){
        // return phpinfo();
        $kecepatan = 50;// 50KM/jam
        $waktuPelayanan = 15;// 15 menit
        $a1 = 0.1;
        $a2 = 0.9;
        $lamda = 1;
        $kapasitas = 14;
        // $maxTime = $this->minutes((Location::orderBy('difference', 'Desc')->first())->difference);
        $maxTime = 480;
        $waktuSetup = 15;//15 Menit
        $kapasitasMoneyCasset = 56;
        $waktuReplenishment = 15;
        $startWork = '07:00:00';
        $timestampstartwork = strtotime($startWork);
        $lasttime = $timestampstartwork;

        $rute = [0];

        $locations = Location::all();
        
        $matrixs = Matrix::all();

        // $minDistance = Matrix::where('origin', $param)->orderBy('distance', 'ASC')->first();


        //Menghitung Waktu Tempuh masing-masing jarak

        foreach($matrixs as $m => $v){
            $time = $v->distance/$kecepatan*60;
            $v['waktu_tempuh'] = ceil($time);
        }

        // return $matrixs;
        // print_r(json_encode($matrixs));

        $next = 1;
        $tourIndex = 0;
        $tour = array();
        $routeInTour = array();
        $allNodesAdded = array();
        $origin = 0;
        $i = 0;
        
        $allNodesAdded[] =$i;

        $totalAllJamkerja = 0;
        do{
            //menentukan jarak terdekat dari depot
            $min = -1;
            $origin = $rute[sizeof($rute)-1];
            $byOrigin = $this->findByOrigin($matrixs, $origin);//Matrix::where('origin', $origin)->get();
            // $rute =[0];

            // echo sizeof(array_unique($allNodesAdded))." ".sizeof($locations)."<br>";
            // print_r(json_encode($allNodesAdded)."<br>");
            // if(sizeof(array_unique($allNodesAdded)) >= sizeof($locations) ) break;

            // echo $origin."<br>";
            foreach($byOrigin as $d => $v){
                if(in_array($v->destination, $rute) || 
                in_array($v->destination, $allNodesAdded)) continue;
                
                // echo $v->destination." <br>";
                if($v->destination != $origin){
                    if($min == -1){
                        $min = $v->distance;
                        // echo $min."index ".$i."<br>";
                        // $next = 1;
                        $i = $v->destination;
                    }
                    else{
                        $tmp = $v->distance;
                        if($min > $tmp){
                            $min = $tmp;
                            $i = $v->destination;
                        }
                        // $next = 1;
                    }
                   
                }
            }
           
            // echo "nilai min : ".$min." index ".$i." = ".$byOrigin[$i]."<br>";

          
            //Pembobotan Jarak dan Waktu Node Sisipan dari Node Bebas Terhadap Node Terdekat	
            if($min != -1){
                $rute[] = $i;//add new route
                $allNodesAdded[] =$i;
            }	
            

            $zJarak = [];
            $zWaktu = [];
            $z12 = [];
            foreach($locations as $l => $v){
                if($l == 0) continue;
                //bobot jarak
                // $tmp = Matrix::where([['origin', $i],['destination', $l] ])->first();
                $dIU = $this->findDistance($matrixs, $i, $l);//$tmp->distance;

                // $tmp = Matrix::where([['origin', $l],['destination', 0] ])->first();
                $dU0 = $this->findDistance($matrixs, $l, 0);//$tmp->distance;

                // $tmp = Matrix::where([['origin', 0],['destination', $i] ])->first();
                $dI0 = $this->findDistance($matrixs, 0, $i);//$tmp->distance;

                $zd = $dIU+$dU0-$dI0;

                $zTmp = (object) array(
                    'node' => $l,
                    'diu' => $dIU,
                    'dU0' => $dU0,
                    'dI0' => $dI0,
                    'zd' => $zd
                );
                $zJarak[] = $zTmp;

                // print_r(json_encode($zJarak));
                // echo "index ".$l." => ".$dIU." - ".$dU0." - ".$dI0." - ".$zd."<br>";

                //end bobot jarak

                //perhitungan bobot waktu
                $zTimetmp = (object) array(
                    'node' => $l,
                    'tiu' => 0,
                    'tU0' => 0,
                    'tI0' => 0,
                    'zt' => 0
                );

                $isloop = 0;
                foreach($matrixs as $m => $mv){
                    // $isloop = 1;
                    if($mv->origin == $i && $mv->destination == $l){
                        // echo "origin ".$i." dest : ".$l." ".$mv->waktu_tempuh;//."<br>";
                        $zTimetmp->tiu = $mv->waktu_tempuh;
                        $isloop++;
                    }
                    if($mv->origin == $l && $mv->destination == 0){
                        $zTimetmp->tU0 = $mv->waktu_tempuh;
                        $isloop++;
                    }
                    if($mv->origin == $i && $mv->destination == 0){
                        $zTimetmp->tI0 = $mv->waktu_tempuh;
                        $isloop++;
                    }

                    
                    $zTimetmp->zt = ($zTimetmp->tiu + $zTimetmp->tU0 + $waktuPelayanan)-$zTimetmp->tI0;
                    if( $isloop >= 3) {
                        $zWaktu[] = $zTimetmp;
                        break;
                    }
                    // echo $zTimetmp->zT."<br>";
                }

                // echo "index ".$l." => ".$zTimetmp->tiu." + ". $zTimetmp->tU0 ."+". $waktuPelayanan."-".$zTimetmp->tI0." = ".$zTimetmp->zt."<br>";

            }

            // print_r(json_encode($zJarak));
            // echo "<br>";
            // print_r($zWaktu);

            //3. Menghitung besarnya ongkos penyisipan yang besarnya proporsional terhadap tambahan jarak dan tambahan waktu tempuh 
            foreach($zJarak as $j => $v){
                // if(in_array($v->node, $allNodesAdded)) continue;
                $z1 = ($a1*$v->zd) + ($a2*$zWaktu[$j]->zt);
                $z2 = ($lamda * $v->dU0)-$z1;
                // echo $z1." ".$z2."<br>";
                $z12[] = (object) array(
                    'node' => $v->node,
                    'z1' => $z1,
                    'z2' => $z2
                );
            }

            // 4. Sisipkan Node Bebas yang memiliki nilai Z1(i,u,j) minimum diantara node i dan node j yang sudah ada

            // print_r($z12);
            $tmpMinZ1 = -1;
            $tmpMinZ2 = -1;
            $iZ1 = 0;
            $iZ2 = 0;
            foreach($z12 as $z => $zv){
                if(in_array($zv->node,$rute) || in_array($zv->node, $allNodesAdded)) {
                    continue;
                }
                // if(in_array($zv->node, $allNodesAdded)) continue;
                if($zv->node == 0) continue;
                if($tmpMinZ1 == -1){
                    $tmpMinZ1 = $zv->z1;
                    $iZ1 = $zv->node;
                    // echo $zv->node." 1<br>";
                }
                else{
                    $tmpZ1 = $zv->z1;

                    if($tmpMinZ1 > $tmpZ1){
                        $tmpMinZ1 = $tmpZ1;
                        $iZ1 = $zv->node;
                        // echo $zv->node." 1b<br>";
                    }
                }
            }
            if(sizeof($rute) <= $kapasitas  && $iZ1 != 0) {
                $rute[] = $iZ1;//add new route
                $allNodesAdded[] =$iZ1;
            }

            foreach($z12 as $z => $zv){
                if(in_array($zv->node,$rute) || in_array($zv->node, $allNodesAdded)) continue;
                
                if($zv->node == 0) continue;

                if($tmpMinZ2 == -1){
                    $tmpMinZ2 = $zv->z2;
                    // echo $tmpMinZ2."<br>";
                    $iZ2 = $zv->node;
                }
                else{
                    $tmpZ2 = $zv->z2;
                    // echo $tmpMinZ2." | ".$tmpZ2."<br>";

                    if($tmpMinZ2 < $tmpZ2){
                        // echo $zv->node." 2<br>";
                        $tmpMinZ2 = $tmpZ2;
                        $iZ2 = $zv->node;
                    }
                }
            }
            
            // echo "nilai min ".$tmpMinZ1." index : ".$iZ2." = ".$byOrigin[$iZ2]."<br>";
            
            if(sizeof($rute) <= $kapasitas && $iZ2 != 0) {
                $rute[] = $iZ2;
                $allNodesAdded[] =$iZ2;
            }

            // print_r(json_encode($rute)."<br>");

            if(sizeof($rute) >= $kapasitas || sizeof(array_unique($allNodesAdded)) >= sizeof($locations) ){
                $tmpKapasitasMoneyCasset = $kapasitasMoneyCasset;
                $totalWaktuPerjalanan = 0;
                $totalJamKerja = 0;
                $detailsRoute = [];

                $rute[] = 0;
                foreach($rute as $r => $rv){// rute sebelum minimasi fungsi tujuan

                    if($rv == 0 && $r == sizeof($rute)-1){
                        $tmpKapasitasMoneyCasset -= $locations[$rv]->money_casset;
                        $tmpWaktuTempuh = $this->findWaktuTempuh($matrixs,$rv, $rute[$r-1]);
                        $waktuKedatangan = date('H:i:s', $lasttime + $tmpWaktuTempuh*60);

                        $lasttime = $lasttime + (($tmpWaktuTempuh+$waktuReplenishment)*60);
                        $waktuKeberangkatan = date('H:i:s', $lasttime);
                        
            
                        $tmpData = (object) array(
                            'lokasi' => $rv,
                            'jadwal30' => $locations[$rv]->less_than,
                            'cash_out' => $locations[$rv]->cash_out, 
                            'waktu_tempuh' => $tmpWaktuTempuh,
                            'waktu_kedatangan' => $waktuKedatangan,
                            'waktu_keberangkatan' => '00:00:00',//$waktuKeberangkatan,
                            'waktu_replenishment' => $waktuReplenishment,
                            'money_casset' => $locations[$rv]->money_casset,
                            'kapasitas_money_casset' => $tmpKapasitasMoneyCasset,
                        );
                    }
                    elseif($rv == 0 ){
                        $tmpData = (object) array(
                            'lokasi' => $rv,
                            'jadwal30' => $locations[$rv]->less_than,
                            'cash_out' => $locations[$rv]->cash_out, 
                            'waktu_tempuh' => 0,
                            'waktu_kedatangan' => 0,
                            'waktu_keberangkatan' => $startWork,
                            'waktu_replenishment' => 0,
                            'money_casset' => 0,
                            'kapasitas_money_casset' => $tmpKapasitasMoneyCasset,
                        );
                    }
                    else{
                        $tmpKapasitasMoneyCasset -= $locations[$rv]->money_casset;
                        $tmpWaktuTempuh = $this->findWaktuTempuh($matrixs,$rv, $rute[$r-1]);
                        $waktuKedatangan = date('H:i:s', $lasttime + $tmpWaktuTempuh*60);

                        $lasttime = $lasttime + (($tmpWaktuTempuh+$waktuReplenishment)*60);
                        $waktuKeberangkatan = date('H:i:s', $lasttime);
                        
            
                        $tmpData = (object) array(
                            'lokasi' => $rv,
                            'jadwal30' => $locations[$rv]->less_than,
                            'cash_out' => $locations[$rv]->cash_out, 
                            'waktu_tempuh' => $tmpWaktuTempuh,
                            'waktu_kedatangan' => $waktuKedatangan,
                            'waktu_keberangkatan' => $waktuKeberangkatan,
                            'waktu_replenishment' => $waktuReplenishment,
                            'money_casset' => $locations[$rv]->money_casset,
                            'kapasitas_money_casset' => $tmpKapasitasMoneyCasset,
                        );
                    }
                    
                    // print_r(json_encode($tmpData)."<br>");
                    $detailsRoute[] = $tmpData;
                }

                $tmpRute = (object) array(
                    'rute' => $rute, 
                    'details' => $detailsRoute,
                );

                // $tour[$tourIndex][] = $tmpRute;

                // print_r(json_encode($tour));


                //Minimasi Fungsi Tujuan
                $isloop =1;
                $i = 1;

                $tmpLasttime = strtotime($startWork)+($waktuReplenishment*60);
                $newRute = [0, $rute[$i]];
                
                $nodeMin = 0;
                $waktuKedatanganMin = '';   
                $waktuKeberangkatanMin = '';
                $lamaPerjalananMin = 0;       
                $newDetailsRoute = [$detailsRoute[0]];      

                $lamaPerjalanan = $this->findDistance($matrixs, $rute[$i-1], $rute[$i]);//Matrix::where([['origin', $rute[$i-1]],['destination', $rute[$i]] ])->first();// dari node ke i
                $lamaPerjalanan = ceil($lamaPerjalanan/$kecepatan*60);
                
                $tmpLasttime += $lamaPerjalanan*60;

                $lastTimeMin = $tmpLasttime;

                $tmpKapasitasMoneyCasset = $kapasitasMoneyCasset;
                $tmpKapasitasMoneyCasset -= $locations[$rute[$i]]->money_casset;
                
                $waktuKedatanganMin = date('H:i:s', strtotime($startWork)+($lamaPerjalanan*60));   
                $waktuKeberangkatanMin = date('H:i:s', $lastTimeMin);

                $lamaPerjalananMin = $lamaPerjalanan;
                $totalWaktuPerjalanan += $lamaPerjalananMin;//tambah total perjalanan 
                $totalJamKerja += $waktuReplenishment;//tambah jam kerja

                $tmpData = (object) array(
                    'lokasi' => $rute[$i],
                    'jadwal30' => $locations[$rute[$i]]->less_than,
                    'cash_out' => $locations[$rute[$i]]->cash_out, 
                    'waktu_tempuh' => $lamaPerjalananMin,
                    'waktu_kedatangan' => $waktuKedatanganMin,
                    'waktu_keberangkatan' => $waktuKeberangkatanMin,
                    'waktu_replenishment' => $waktuReplenishment,
                    'money_casset' => $locations[$rute[$i]]->money_casset,
                    'kapasitas_money_casset' => $tmpKapasitasMoneyCasset,
                );

                $newDetailsRoute[] = $tmpData;

                // print_r(json_encode($newDetailsRoute));
                while($isloop){
                    $tmpMin = 100;
                    for($j = 2; $j < sizeof($rute)-1; $j++){
                        if(in_array($rute[$j], $newRute)) continue;
                        // if(sizeof($newRute) == 1){
                            if($j == 2 && $tmpMin == 100){
                                // $lamaPerjalanan = $this->findWaktuTempuh($matrixs,$rute[$i], $rute[$j]);
                                
                                $tmp = $this->findWaktuFromNode($detailsRoute,$rute[$j]);
                                $lamaPerjalanan = $this->findDistance($matrixs, $rute[$i], $rute[$j]);//Matrix::where([['origin', $rute[$i]],['destination', $rute[$j]] ])->first();// dari node ke i
                                $lamaPerjalanan = ceil($lamaPerjalanan/$kecepatan*60);

                                $waktuKedatangan = date('H:i:s', $tmpLasttime + $lamaPerjalanan*60);
                                // echo "from ".$rute[$i]." node : ".$rute[$j]." ".$lamaPerjalanan." ";

                                $bobot = $this->findBobotWaktuMinimasi($tmp->jadwal30, $tmp->cash_out,  $waktuKedatangan);

                                $res = ($lamaPerjalanan*1) + (10*$bobot);

                                // print_r(json_encode($res)."<br>");
                                // break;

                                $tmpMin = $res;
                                $nodeMin = $j;
                                $lastTimeMin = $tmpLasttime + $lamaPerjalanan*60;

                                $waktuKedatanganMin = $waktuKedatangan;   
                                $waktuKeberangkatanMin = date('H:i:s', $lastTimeMin+$waktuReplenishment*60);
                                $lamaPerjalananMin = $lamaPerjalanan;
                            }
                            else if($tmpMin == 100){
                                $tmp = $this->findWaktuFromNode($detailsRoute,$rute[$j]);
                                $lamaPerjalanan = $this->findDistance($matrixs, $rute[$i], $rute[$j]);//Matrix::where([['origin', $rute[$i]],['destination', $rute[$j]] ])->first();// dari node ke i
                                $lamaPerjalanan = ceil($lamaPerjalanan/$kecepatan*60);

                                $waktuKedatangan = date('H:i:s', $tmpLasttime + $lamaPerjalanan*60);
                                // echo "from ".$rute[$i]." node : ".$rute[$j]." ".$lamaPerjalanan." ";

                                $bobot = $this->findBobotWaktuMinimasi($tmp->jadwal30, $tmp->cash_out,  $waktuKedatangan);

                                $res = ($lamaPerjalanan*1) + (10*$bobot);

                                // print_r(json_encode($res)."<br>");
                                // break;

                                $tmpMin = $res;
                                $nodeMin = $j;
                                $lastTimeMin = $tmpLasttime + $lamaPerjalanan*60;

                                $waktuKedatanganMin = $waktuKedatangan;   
                                $waktuKeberangkatanMin = date('H:i:s', $lastTimeMin+$waktuReplenishment*60);
                                $lamaPerjalananMin = $lamaPerjalanan;
                            }
                            else{
                                $tmp = $this->findWaktuFromNode($detailsRoute,$rute[$j]);
                                $lamaPerjalanan = $this->findDistance($matrixs, $rute[$i], $rute[$j]);//Matrix::where([['origin', $rute[$i]],['destination', $rute[$j]] ])->first();// dari node ke i
                                $lamaPerjalanan = ceil($lamaPerjalanan/$kecepatan*60);

                                $waktuKedatangan = date('H:i:s', $tmpLasttime + $lamaPerjalanan*60);
                                // echo "from ".$rute[$i]." node : ".$rute[$j]." ".$lamaPerjalanan." ";

                                $bobot = $this->findBobotWaktuMinimasi($tmp->jadwal30, $tmp->cash_out,  $waktuKedatangan);

                                $res = ($lamaPerjalanan*1) + (10*$bobot);

                                // print_r(json_encode($res)."<br>");

                                if($res < $tmpMin){
                                    $tmpMin = $res;
                                    $nodeMin = $j;
                                    $lastTimeMin = $tmpLasttime + $lamaPerjalanan*60;

                                    $waktuKedatanganMin = $waktuKedatangan;   
                                    $waktuKeberangkatanMin = date('H:i:s', $lastTimeMin+$waktuReplenishment*60);
                                    $lamaPerjalananMin = $lamaPerjalanan;
                                }

                            }
                        // }
                    }

                    $i = $nodeMin;
                    $newRute[] = $rute[$i];
                    
                    // $time = date('H:i:s', $lastTimeMin+$waktuReplenishment*60);
                    // echo $time;
                    $tmpLasttime = $lastTimeMin+$waktuReplenishment*60;
                    $totalWaktuPerjalanan += $lamaPerjalananMin;//tambah total perjalanan 
                    $totalJamKerja += $waktuReplenishment;//tambah jam kerja

                    // echo "node min : ".$i." ".$rute[$i]." -> ".$tmpMin."<br>";

                    $tmpKapasitasMoneyCasset -= $locations[$rute[$i]]->money_casset;

                    $tmpData = (object) array(
                        'lokasi' => $rute[$i],
                        'jadwal30' => $locations[$rute[$i]]->less_than,
                        'cash_out' => $locations[$rute[$i]]->cash_out, 
                        'waktu_tempuh' => $lamaPerjalananMin,
                        'waktu_kedatangan' => $waktuKedatanganMin,
                        'waktu_keberangkatan' => $waktuKeberangkatanMin,
                        'waktu_replenishment' => $waktuReplenishment,
                        'money_casset' => $locations[$rute[$i]]->money_casset,
                        'kapasitas_money_casset' => $tmpKapasitasMoneyCasset,
                    );

                    $newDetailsRoute[] = $tmpData;//add temporary Details route;

                    // print_r(json_encode($tmpData));

                    if(sizeof($newRute) == sizeof($rute)-1){
                        $lamaPerjalanan = $this->findDistance($matrixs, 0, $rute[$i]);//Matrix::where([['origin', $rute[$i]],['destination', 0] ])->first();// dari node ke i
                        $lamaPerjalanan = ceil($lamaPerjalanan/$kecepatan*60);
                        $totalWaktuPerjalanan += $lamaPerjalanan;
                        $totalJamKerja += $totalWaktuPerjalanan;//+$waktuReplenishment;
                        $newRute[] = 0;

                        $waktuKedatangan = date('H:i:s', $tmpLasttime + $lamaPerjalanan*60);

                        $tmpData = (object) array(
                            'lokasi' => 0,
                            'jadwal30' => $locations[0]->less_than,
                            'cash_out' => $locations[0]->cash_out, 
                            'waktu_tempuh' => $lamaPerjalanan,
                            'waktu_kedatangan' => $waktuKedatangan,
                            'waktu_keberangkatan' => '00:00:00',
                            'waktu_replenishment' => 0,//$waktuReplenishment,
                            'money_casset' => $locations[0]->money_casset,
                            'kapasitas_money_casset' => $tmpKapasitasMoneyCasset,
                        );

                        $newDetailsRoute[] = $tmpData;

                        $tmpRute = (object) array(
                            'rute' => $rute, 
                            'details' => $detailsRoute,
                            'newRute' => $newRute,
                            'newDetailsRute' => $newDetailsRoute,
                            'totalWaktuPerjalanan' => $totalWaktuPerjalanan,
                            'totalJamKerja' => $totalJamKerja,
                            'jarakTempuh' => $totalWaktuPerjalanan*$kecepatan/60
                        );

                        
                        // $allNodesAdded = array_unique($allNodesAdded);

                        $totalAllJamkerja +=$totalJamKerja;

                        $tour[$tourIndex][] = $tmpRute;

                        // if($totalAllJamkerja >= $maxTime) {
                        //     echo "tur baru";
                        //     $tourIndex++;
                        //     $totalAllJamkerja = 0;
                        // }

                        // print_r(json_encode($newRute));
                        // print_r(json_encode($newDetailsRoute));
                        break;
                    }
                }
                

                $rute = [0];
                if($totalAllJamkerja >= $maxTime) {
                    // echo "tur baru";
                    $tourIndex++;
                    $totalAllJamkerja = 0;
                    $startWork = '07:00:00';
                    $lasttime = strtotime($startWork);
                    // $i = 0;
                }
                else{
                    $i = 0;
                    // echo " new rute<br>";
                    $startWork = date('H:i:s', strtotime($waktuKedatangan) + $waktuSetup*60);
                    $lasttime = strtotime($startWork);
                }

                
                // $next--;
                if(sizeof(array_unique($allNodesAdded)) >= sizeof($locations) ) break;
            }
        }while($next);
        
        // print_r(json_encode($allNodesAdded)."<br>");
        // print_r(json_encode($tour));
        


        //Hasil Minimasi Fungsi TUjuan 

        foreach($tour as $t => $tv){
            echo "<h3>Tour Ke ".($t+1)."</h3>";
            // print_r(json_encode($tv));
            foreach($tv as $r => $rv){
                echo "<h4>Rute Ke ".($r+1)."</h4>";
                echo "<table border='1' style='border-collapse: collapse'><tr>
                <th>Lokasi</th>
                <th>Jadwal Pengiriman 30%</th>
                <th>Jadwal Cashout</th>
                <th>Lama Perjalanan (Menit)</th>
                <th>Waktu Kedatangan</th>
                <th>Waktu Proses Replenishment (Menit)</th>
                <th>Waktu Keberangkatan Kendaraan</th>
                <th>Money Casset Terpakai</th>
                <th>Kapasitas Money Casset</th>";
                echo "<tbody>";
                foreach($rv->newDetailsRute as $d => $dv){
                    echo "<tr>
                    <td>".$dv->lokasi."</td>
                    <td>".$dv->jadwal30."</td>
                    <td>".$dv->cash_out."</td>
                    <td>".$dv->waktu_tempuh."</td>
                    <td>".$dv->waktu_kedatangan."</td>
                    <td>".$dv->waktu_replenishment."</td>
                    <td>".$dv->waktu_keberangkatan."</td>
                    <td>".$dv->money_casset."</td>
                    <td>".$dv->kapasitas_money_casset."</td></tr>";
                }
                echo "</tbody>";
                echo "<tfoot>
                <tr>
                <td colspan='2'>Total Perjalanan (Menit)</td>
                <td >".$rv->totalWaktuPerjalanan."</td>
                <td colspan='2'>Total Jam Kerja (Menit)</td>
                <td >".$rv->totalJamKerja."</td>
                <td></td>
                <td></td>
                <td></td>
                </tr>
                <tr>
                <td colspan='2'>Total Jarak Tempuh (Km)</td>
                <td>".$rv->jarakTempuh."</td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                </tr>
                </tfoot></table>";
                // foreach($tv-> )
            }
           
           
        }
        return null;
        return view('page.calculate.index');
    }

    function minutes($time){
        $time = explode(':', $time);
        return ($time[0]*60) + ($time[1]) + ($time[2]/60);
    }

    function findWaktuTempuh($matrixs, $from, $to){
        foreach($matrixs as $m => $mv){
            if($mv->origin == $from && $mv->destination == $to){
                return $mv->waktu_tempuh;
            }
        }
    }
    
    function findWaktuFromNode($detailsRoute, $node){
        foreach($detailsRoute as $d => $dv){
            if($dv->lokasi == $node) return $dv;
        }
    }

    function findBobotWaktuMinimasi($lessthan, $cashout, $waktuKedatangan){
        $tmpLesthan = strtotime($lessthan);
        $tmpCashout = strtotime($cashout);
        $tmpKedatangan = strtotime($waktuKedatangan);

// echo $lessthan." ".$waktuKedatangan." = ". $tmpLesthan-$tmpKedatangan;//." ".($tmpKedatangan >= $tmpLesthan && $tmpKedatangan < $tmpCashout);

$bobot = 0;
        if(($tmpLesthan - $tmpKedatangan) > 3600) $bobot = 3; //kedatangan > 1jam sebelum 30%
        if(($tmpLesthan - $tmpKedatangan) <= 3600) $bobot = 2; //kedatangan <= 1jam sebelum 30%
        if($tmpKedatangan >= $tmpLesthan && $tmpKedatangan < $tmpCashout) $bobot = 1;
        if($tmpKedatangan == $tmpCashout) $bobot = 0;
// echo " ".$bobot." ";
        return $bobot;
    }

    function findDistance($matrix, $origin, $destination){
        foreach($matrix as $m){
            if($m->origin == $origin && $m->destination == $destination){
                return $m->distance;
            }
            elseif($m->origin == $destination && $m->destination == $origin){
                return $m->distance;
            }
        }
    }

    function findByOrigin($matrix, $origin){
        $temp = [];
        foreach($matrix as $m){
            if($m->origin == $origin){
                $temp[] = $m;
            }
        }

        return $temp;
    }
}






