<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Matrix;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Facades\DB;
use Alert;

class MatrixController extends Controller
{
    public function index(Request $request){
        if ($request->ajax()) {
            $data = DB::select("SELECT m.id, d.location AS destination, m.distance, o.location AS origin FROM matrics as m
            LEFT JOIN locations as o on o.node = m.origin
            LEFT JOIN locations as d on d.node = m.destination");
            return Datatables::of($data)
                    ->addIndexColumn()
                    ->make(true);
        }

        return view('page.master.matrix');
    }

    public function destroy(){
        DB::table('matrics')->delete();

        Alert::success('Success', 'Matrik berhasil dihapus!');
        return redirect()->back();
    }
}
