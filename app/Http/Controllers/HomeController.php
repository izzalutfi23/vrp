<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Imports\{HelpImport, LocationImport};
use Maatwebsite\Excel\Facades\Excel;
use App\Http\Controllers\Controller;
use App\Models\{Matrix, Location};
use Alert;
use Illuminate\Support\Facades\DB;


class HomeController extends Controller
{
    public function index(){
        return view('home');
    }

    // public function import(Request $request) 
	// {
	// 	// validasi
	// 	$this->validate($request, [
	// 		'file' => 'required|mimes:csv,xls,xlsx'
	// 	]);

	// 	$file = $request->file('file');

	// 	$nama_file = rand().$file->getClientOriginalName();

	// 	$file->move('file',$nama_file);

	// 	Excel::import(new HelpImport, public_path('/file/'.$nama_file));

    //     Alert::success('Lokasi berhasil diimport!');
	// 	return redirect()->back();
	// }

    public function convert(){
        // $locations = Location::all();
        $locations = DB::select("SELECT node, value FROM locations");

        foreach($locations as $loc){
            $arr = json_decode($loc->value);
            foreach($arr as $key => $v){
                // $index = Location::select('id', 'node', 'location')->where('node', $key)->first();
                $index = collect(DB::select("SELECT id, node, location FROM locations WHERE node='$key'"))->first();
                Matrix::create([
                    'origin' => $loc->node,
                    'destination' => $index->node,
                    'distance' => $v
                ]);
            }
        }

        Alert::success('Success', 'Matrik berhasil diconvert!');
		return redirect()->back();
    }

    public function location(){
        return view('location');
    }

    public function locImport(Request $request){
        // validasi
		$this->validate($request, [
			'file' => 'required|mimes:csv,xls,xlsx'
		]);

		$file = $request->file('file');

		$nama_file = rand().$file->getClientOriginalName();

		$file->move('file',$nama_file);

		Excel::import(new LocationImport, public_path('/file/'.$nama_file));

		Alert::success('Success', 'Lokasi berhasil diimport!');
		return redirect()->back();
    }
}
