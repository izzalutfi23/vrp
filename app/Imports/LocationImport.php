<?php

namespace App\Imports;

use App\Models\Location;
use Maatwebsite\Excel\Concerns\ToModel;

class LocationImport implements ToModel
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        $data = [];
        $no = 1;
        foreach($row as $r){
            if($no > 6){
                array_push($data, $r);
            }
            $no++;
        }

        $less = explode(".",$row[3]);
        $cash = explode(".",$row[4]);
        $lessTime = date('H:i', strtotime($less[0].':'.$less[1]));
        $cashTime = date('H:i', strtotime($cash[0].':'.$cash[1]));
        $diff = $this->getTimeDiff($lessTime, $cashTime);
        return new Location([
            'node' => $row[1],
            'location' => $row[0],
            'money_casset' => $row[2],
            'less_than' => $less[0].':'.$less[1],
            'cash_out' => $cash[0].':'.$cash[1],
            'difference' => $diff,
            'value' => json_encode($data)
        ]);
    }

    public function getTimeDiff($dtime,$atime)
    {
        $nextDay = $dtime>$atime?1:0;
        $dep = explode(':',$dtime);
        $arr = explode(':',$atime);
        $diff = abs(mktime($dep[0],$dep[1],0,date('n'),date('j'),date('y'))-mktime($arr[0],$arr[1],0,date('n'),date('j')+$nextDay,date('y')));
        $hours = floor($diff/(60*60));
        $mins = floor(($diff-($hours*60*60))/(60));
        $secs = floor(($diff-(($hours*60*60)+($mins*60))));
        if(strlen($hours)<2){$hours="0".$hours;}
        if(strlen($mins)<2){$mins="0".$mins;}
        if(strlen($secs)<2){$secs="0".$secs;}
        return $hours.':'.$mins.':'.$secs;
    }

}
