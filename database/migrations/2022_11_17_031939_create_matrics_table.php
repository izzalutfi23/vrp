<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('matrics', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('origin');
            $table->foreign('origin')->references('node')->on('locations')->onUpdate('cascade')->onDelete('cascade');
            $table->integer('destination');
            $table->foreign('destination')->references('node')->on('locations')->onUpdate('cascade')->onDelete('cascade');
            $table->double('distance');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('matrics');
    }
};
