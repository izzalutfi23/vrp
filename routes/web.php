<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Route::controller('App\Http\Controllers\HomeController')->group(function(){
    // Route::get('/', 'index');
    Route::post('/import', 'import')->name('import');
    Route::get('/convert', 'convert')->name('convert');

    // Location
    Route::get('/location', 'location')->name('location');
    Route::post('/location', 'locImport')->name('location.post');
});


// This App
Route::controller('App\Http\Controllers\DashboardController')->group(function(){
    Route::get('/', 'index')->name('dashboard');
});

// Calculate
Route::controller('App\Http\Controllers\CalculateController')->group(function(){
    Route::get('/calculate', 'index')->name('calculate');
});

// Hasil
Route::controller('App\Http\Controllers\ResultController')->group(function(){
    Route::get('/result', 'index')->name('result');
});

// Master
Route::group(['prefix' => 'master'], function(){
    // Location
    Route::controller('App\Http\Controllers\LocationController')->group(function(){
        Route::get('/location', 'index')->name('master.location');
        Route::get('/location/delete/all', 'destroyAll')->name('master.location.delete.all');
        Route::post('/location', 'store')->name('master.location.store');
        Route::patch('/location/distance/{id}', 'storeDistance')->name('master.location.distance');
        Route::get('/location/delete/{id}', 'destroy')->name('master.location.delete');
    });

    // Matrix
    Route::controller('App\Http\Controllers\MatrixController')->group(function(){
        Route::get('/matrix', 'index')->name('master.matrix');
        Route::get('/matrix/delete', 'destroy')->name('master.matrix.delete');
    });
});
